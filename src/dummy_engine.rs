// SPDX-FileCopyrightText: 2022 Thomas Kramer
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! DRC engine which never finds any rule violations.

use super::*;

/// Dummy design-rule check engine which never finds any design rule violations.
/// Used as mock DRC checker for tests.
pub struct DummyDrcEngine;

impl<LN: db::L2NBase> DrcEngine<LN> for DummyDrcEngine {
    type DrcResult = DummyDrcResult;
    type Error = ();

    fn check(_chip: &LN, _top_cell: &LN::CellId) -> Result<Self::DrcResult, Self::Error> {
        Ok(DummyDrcResult)
    }

    fn flat_check(
        &self,
        _layer_data: &HashMap<LN::LayerId, Vec<(db::SimpleRPolygon<LN::Coord>, Option<LN::NetId>)>>,
    ) -> Result<Self::DrcResult, Self::Error> {
        Ok(DummyDrcResult)
    }
}

/// Return type of a dummy DRC engine which never finds any violations.
pub struct DummyDrcResult;

impl<LN: db::L2NBase> DrcResult<LN> for DummyDrcResult {
    fn num_violations(&self) -> usize {
        0
    }

    fn markers(&self) -> Vec<(LN::LayerId, db::Rect<LN::Coord>)> {
        vec![]
    }
}
