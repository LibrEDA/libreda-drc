// SPDX-FileCopyrightText: 2022 Thomas Kramer
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Check interactions between pairs of edges.

use super::drc_marker::DrcMarker;

/// Relation type between edges.
#[derive(Copy, Clone, Debug, Hash, Eq, PartialEq)]
#[allow(dead_code)]
pub enum EdgeRelation {
    /// Spacing between edges of two polygons.
    Spacing,
    /// Width of a polygon.
    Width,
    /// Overlap of one polygon over the other.
    Overlap,
    /// Enclosure of the first polygon inside the second.
    Enclosure,
}

/// Check for closeness or interactions between edges.
/// The trait is meant to be used to implement different distance metrics (projective,
/// 1-norm/rectilinear and 2-norm/euclidean).
pub(crate) trait EdgeInteractionChecker {
    /// Supported edge type.
    type Edge: Copy;

    /// Get the segment of the edge `b` which is near or interacts with edge `a`.
    fn near_edge(&self, a: &Self::Edge, b: &Self::Edge) -> Option<Self::Edge>;

    /// Edge relation type which is checked by this interaction checker.
    fn edge_relation_type(&self) -> EdgeRelation;

    fn check(&self, a: &Self::Edge, b: &Self::Edge) -> Option<DrcMarker<Self::Edge>> {
        self.near_edge(a, b).map(|b_violating_part| DrcMarker {
            drc_type: self.edge_relation_type(),
            edge1: *a,
            edge2: b_violating_part,
        })
    }
}
