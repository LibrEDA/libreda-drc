// SPDX-FileCopyrightText: 2022 Thomas Kramer
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#![deny(missing_docs)]

//! Geometrical design rule checks for the LibrEDA framework.

extern crate iron_shapes;
extern crate num_traits;

use std::collections::HashMap;

use libreda_db::prelude as db;

mod drc_marker;
mod dummy_engine;
mod edge_checks;
mod horizontal_edge;
mod redge_checks;
mod scanline;
mod simple_engine;

pub use dummy_engine::*;
pub use simple_engine::*;

/// Design-rule check engine.
pub trait DrcEngine<LN: db::L2NBase> {
    /// Datatype holding the result of the DRC check.
    type DrcResult: DrcResult<LN>;

    /// Datatype returned when the DRC algorithm cannot run successfully.
    type Error;

    /// Check the top cell for design rule violations.
    fn check(chip: &LN, top_cell: &LN::CellId) -> Result<Self::DrcResult, Self::Error>;

    /// Check the given layers for design rule violations.
    fn flat_check(
        &self,
        layer_data: &HashMap<LN::LayerId, Vec<(db::SimpleRPolygon<LN::Coord>, Option<LN::NetId>)>>,
    ) -> Result<Self::DrcResult, Self::Error>;
}

/// Simple functions which should be supported by DRC results.
pub trait DrcResult<LN: db::L2NBase> {
    /// Get the number of detected design rule violations.
    fn num_violations(&self) -> usize;

    /// Mark the locations of violations with rectangles.
    fn markers(&self) -> Vec<(LN::LayerId, db::Rect<LN::Coord>)>;
}
