// SPDX-FileCopyrightText: 2022 Thomas Kramer
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Data structure for horizontal edges.

use libreda_db::prelude as db;
use std::cmp::Ordering;

/// Horizontal edge with an associated ID value.
#[derive(Copy, Clone, Debug, Hash)]
pub(crate) struct HorizontalEdge<T, ID = ()> {
    /// Lower x coordinate.
    pub x_start: T,
    /// Higher x coordinate.
    pub x_end: T,
    /// y coordinate.
    pub y: T,
    /// User-supplied identifier of the edge.
    /// Associates the edge with a geometric object or an ID thereof.
    pub edge_id: ID,
}

impl<T: PartialEq, ID> Eq for HorizontalEdge<T, ID> {}

impl<T: PartialEq, ID> PartialEq for HorizontalEdge<T, ID> {
    fn eq(&self, other: &Self) -> bool {
        self.x_start == other.x_start && self.x_end == other.x_end && self.y == other.y
    }
}

impl<T: Ord + PartialEq, ID> Ord for HorizontalEdge<T, ID> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.x_start
            .cmp(&other.x_start)
            .then(self.x_end.cmp(&other.x_end))
            .then(self.y.cmp(&other.y))
    }
}

impl<T: PartialOrd + PartialEq, ID> PartialOrd for HorizontalEdge<T, ID> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(
            self.x_start
                .partial_cmp(&other.x_start)
                .unwrap()
                .then(self.x_end.partial_cmp(&other.x_end).unwrap())
                .then(self.y.partial_cmp(&other.y).unwrap()),
        )
    }
}

impl<T: Copy, ID> Into<db::REdge<T>> for &HorizontalEdge<T, ID> {
    fn into(self) -> db::REdge<T> {
        db::REdge::new_raw(
            self.x_start,
            self.x_end,
            self.y,
            db::REdgeOrientation::Horizontal,
        )
    }
}

impl<T, ID> Into<db::REdge<T>> for HorizontalEdge<T, ID> {
    fn into(self) -> db::REdge<T> {
        db::REdge::new_raw(
            self.x_start,
            self.x_end,
            self.y,
            db::REdgeOrientation::Horizontal,
        )
    }
}

impl<T: PartialOrd> HorizontalEdge<T> {
    /// Create a new horizontal edge.
    pub fn new(x_start: T, x_end: T, y: T) -> Self {
        Self {
            x_start,
            x_end,
            y,
            edge_id: (),
        }
    }
}

impl<T: PartialOrd, ID> HorizontalEdge<T, ID> {
    /// Create a new horizontal edge.
    pub fn new_with_id(x_start: T, x_end: T, y: T, id: ID) -> Self {
        Self {
            x_start,
            x_end,
            y,
            edge_id: id,
        }
    }
}
