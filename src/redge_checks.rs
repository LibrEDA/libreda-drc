// SPDX-FileCopyrightText: 2022 Thomas Kramer
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Check interactions between pairs of rectilinear edges.

use db::traits::*;
use libreda_db::prelude as db;

use super::edge_checks::*;

/// Check interactions between rectilinear edges.
#[derive(Copy, Clone, Debug, Hash, PartialEq, Eq)]
pub struct REdgeInteractionChecker<Coord> {
    pub(crate) relation_type: EdgeRelation,
    /// Distance component which is orthogonal to the reference edge.
    pub(crate) orthogonal_distance: Coord,
    /// Distance component which is parallel to the reference edge.
    pub(crate) projected_distance: Coord,
}

impl<Coord> EdgeInteractionChecker for REdgeInteractionChecker<Coord>
where
    Coord: db::CoordinateType,
{
    type Edge = db::REdge<Coord>;

    /// Get the segment of the edge `b` which conflicts with edge `a`.
    fn near_edge(&self, a: &db::REdge<Coord>, b: &db::REdge<Coord>) -> Option<db::REdge<Coord>> {
        match self.relation_type {
            EdgeRelation::Spacing => {
                near_edge(a, b, self.orthogonal_distance, self.projected_distance)
            }
            EdgeRelation::Width => {
                // Adapt edge directions to turn the spacing-check into a width-check.
                let a_rev = a.reversed();
                let b_rev = b.reversed();

                near_edge(
                    &a_rev,
                    &b_rev,
                    self.orthogonal_distance,
                    self.projected_distance,
                )
                // Undo reversing of the edge direction.
                .map(|e| e.reversed())
            }
            EdgeRelation::Enclosure => {
                // Adapt edge directions to turn the spacing-check into a enclosure-check.
                let a_rev = a.reversed();
                near_edge(
                    &a_rev,
                    &b,
                    self.orthogonal_distance,
                    self.projected_distance,
                )
            }
            EdgeRelation::Overlap => {
                todo!("overlap check")
            }
        }
    }

    fn edge_relation_type(&self) -> EdgeRelation {
        self.relation_type
    }
}

/// Get the part of the edge `b` which is closer than `distance` to edge `a`
/// AND is on the right side of `a`.
///
/// Returns `None` if `b` is not interacting with `a` or if `a` has zero-length.
///
/// Example of a spacing check. `projected_distance` is 0:
/// ```txt
///               returned part of b
///             /
///        b ===========--------------->
///
///   <-----------------  a
///
/// ```
///
/// Example of a spacing check. `projected_distance` is > 0:
/// ```txt
///               returned part of b
///             /
///        b =================--------->
///
///   <-----------------  a
///
/// ```
///
/// # Parameters
/// `orthogonal_distance`: Distance component which is orthogonal to `a`.
/// `projected_distance`: Distance component which is parallel to `a`.
fn near_edge<T>(
    a: &db::REdge<T>,
    b: &db::REdge<T>,
    orthogonal_distance: T,
    projected_distance: T,
) -> Option<db::REdge<T>>
where
    T: db::CoordinateType,
{
    if a.is_degenerate() {
        // Direction of a cannot be determined.
        None
    } else {
        // Construct 'keep-out' rectangle which starts at the edge `a` and is extended towards the right of the edge
        // by `orthogonal_distance` and towards front and back of the edge by `projected_distance`.
        debug_assert!(!a.is_degenerate());
        let a_dir = a.direction().unwrap(); // Unwrap is safe because a is known not to be degenerate.
        let a_normal_vector = a_dir.rotate_ortho(db::Angle::R270);

        let keep_out_rect = {
            let p1 = a.start() - a_dir * projected_distance;
            let p2 = a.end() + a_dir * projected_distance + a_normal_vector * orthogonal_distance;
            db::Rect::new(p1, p2)
        };

        // Compute intersection of keep-out-rectangle and the edge `b`.
        let intersection = {
            let intersection = {
                let r = db::Rect::new(b.start(), b.end()); // This is a zero-width rectangle (the edge is axis aligned).
                keep_out_rect.intersection_inclusive_bounds(&r)
            };

            intersection.and_then(|intersection| {
                let intersection_is_on_boundary = {
                    let i_ll = intersection.lower_left();
                    let i_ur = intersection.upper_right();
                    let k_ll = keep_out_rect.lower_left();
                    let k_ur = keep_out_rect.upper_right();

                    (i_ll.x == k_ll.x && i_ur.x == k_ll.x)
                        || (i_ll.x == k_ur.x && i_ur.x == k_ur.x)
                        || (i_ll.y == k_ll.y && i_ur.y == k_ll.y)
                        || (i_ll.y == k_ur.y && i_ur.y == k_ur.y)
                };

                if intersection_is_on_boundary {
                    None
                } else {
                    let e = db::REdge::try_from_points(intersection.lower_left(), intersection.upper_right())
                        .unwrap(); // Unwrap is safe because intersection must be a zero-with rectangle, hence the two points must be on a horizontal or vertical line.

                    debug_assert!(e.offset == b.offset, "offset of intersection should be the same as for the original edge");
                    debug_assert!(e.orientation == b.orientation, "orientation of the intersection should be the same as of the original edge");

                    // Restore original orientation.
                    let e = if (e.start < e.end) != (b.start < b.end) {
                        e.reversed()
                    } else {
                        e
                    };
                    Some(e)
                }
            })
        };

        intersection
    }
}

#[test]
fn test_near_edge_parallel_edges() {
    let e1 = db::REdge::try_from_points((1i32, 2), (2, 2)).unwrap();
    let e2 = db::REdge::try_from_points((0, 0), (10, 0)).unwrap();

    // No interaction.
    let near = near_edge(&e1, &e2, 2, 0);
    assert_eq!(near, None);

    let near = near_edge(&e1, &e2, 3, 0);
    assert_eq!(
        near,
        Some(db::REdge::try_from_points((1, 0), (2, 0)).unwrap())
    );

    let near = near_edge(&e1, &e2, 3, 1);
    assert_eq!(
        near,
        Some(db::REdge::try_from_points((0, 0), (3, 0)).unwrap())
    );

    let near = near_edge(&e1, &e2, 3, 2);
    assert_eq!(
        near,
        Some(db::REdge::try_from_points((0, 0), (4, 0)).unwrap())
    );
}

#[test]
fn test_near_edge_orthogonal_edges() {
    let e1 = db::REdge::try_from_points((0i32, -1), (0, 10)).unwrap();
    let e2 = db::REdge::try_from_points((2, 0), (10, 0)).unwrap();

    // No interaction.
    let near = near_edge(&e1, &e2, 2, 0);
    assert_eq!(near, None);

    let near = near_edge(&e1, &e2, 3, 0);
    assert_eq!(
        near,
        Some(db::REdge::try_from_points((2, 0), (3, 0)).unwrap())
    );
}

#[test]
fn test_spacing_check() {
    use super::drc_marker::DrcMarker;
    use super::horizontal_edge::HorizontalEdge;

    let spacing_checker = REdgeInteractionChecker {
        relation_type: EdgeRelation::Spacing,
        orthogonal_distance: 10i32,
        projected_distance: 0,
    };

    let e1 = HorizontalEdge::new(2, 0, 0).into();
    let e2 = HorizontalEdge::new(1, 3, 9).into();
    let e_far_away = HorizontalEdge::new(1, 3, 100).into();

    let drc_marker = spacing_checker.check(&e1, &e2);

    assert_eq!(
        drc_marker,
        Some(DrcMarker {
            drc_type: EdgeRelation::Spacing,
            edge1: e1,
            edge2: HorizontalEdge::new(1, 2, 9).into(),
        })
    );

    assert_eq!(spacing_checker.check(&e1, &e_far_away), None);
}

#[test]
fn test_width_check() {
    use super::drc_marker::DrcMarker;
    use super::horizontal_edge::HorizontalEdge;

    let spacing_checker = REdgeInteractionChecker {
        relation_type: EdgeRelation::Width,
        orthogonal_distance: 2i32,
        projected_distance: 0,
    };

    let e1 = HorizontalEdge::new(0, 2, 0).into();
    let e_narrow = HorizontalEdge::new(3, 1, 1).into();
    let e_wide = HorizontalEdge::new(3, 1, 10).into();

    let drc_marker = spacing_checker.check(&e1, &e_narrow);

    assert_eq!(
        drc_marker,
        Some(DrcMarker {
            drc_type: EdgeRelation::Width,
            edge1: e1,
            edge2: HorizontalEdge::new(2, 1, 1).into(),
        })
    );

    assert_eq!(spacing_checker.check(&e1, &e_wide), None);
}

#[test]
fn test_enclosure_check() {
    use super::drc_marker::DrcMarker;
    use super::horizontal_edge::HorizontalEdge;

    let enclosure_checker = REdgeInteractionChecker {
        relation_type: EdgeRelation::Enclosure,
        orthogonal_distance: 2i32,
        projected_distance: 0,
    };

    let inside = HorizontalEdge::new(0, 2, 0).into();
    let outside_close = HorizontalEdge::new(1, 3, 1).into();
    let outside_far = HorizontalEdge::new(1, 3, 10).into();

    let drc_marker = enclosure_checker.check(&inside, &outside_close);

    assert_eq!(
        drc_marker,
        Some(DrcMarker {
            drc_type: EdgeRelation::Enclosure,
            edge1: inside,
            edge2: HorizontalEdge::new(1, 2, 1).into(),
        })
    );

    assert_eq!(enclosure_checker.check(&inside, &outside_far), None);
}
