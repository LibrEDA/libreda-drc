// SPDX-FileCopyrightText: 2022 Thomas Kramer
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Markers for DRC violations.

use super::edge_checks::EdgeRelation;

/// Mark a design rule violation with the two involved edges (or parts of the edges)
/// and the type of check.
#[derive(Copy, Clone, Debug, Hash, Eq, PartialEq)]
pub struct DrcMarker<Edge> {
    /// Type of edge relation which is violated.
    pub drc_type: EdgeRelation,
    /// First edge.
    pub edge1: Edge,
    /// Second edge (or part of it).
    pub edge2: Edge,
}
