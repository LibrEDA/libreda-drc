// SPDX-FileCopyrightText: 2022 Thomas Kramer
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Scanline data-structure for processing horizontal edges.
//! Vertical edges can processed as well but must be rotated first.
//!
//! The edges must come from well-formed rectilinear polygons without any self intersections and
//! overlaps.

#![allow(unused)] // TODO: Remove when stable.

use libreda_db::prelude as db;

use num_traits::Num;

use std::cmp::Ordering;
use std::collections::{BTreeSet, BinaryHeap};
use std::ops::Bound;

use super::drc_marker::DrcMarker;
use super::edge_checks;
use super::edge_checks::EdgeInteractionChecker;
use super::horizontal_edge::*;
use iron_shapes::CoordinateType;

#[derive(Copy, Clone, Debug, Hash)]
struct ScanlineEvent<T, ID> {
    edge: HorizontalEdge<T, ID>,
    /// Virtual extension of the edge in both direction.
    /// Makes sure the edge stays in the scan line for long enough to also detect edge conflicts
    /// of edges which are not overlapping in their projection.
    horizontal_extension: T,
    is_left_event: bool,
    /// This ID is shared with the other event.
    event_pair_id: usize,
}

// impl<T: Eq, ID> Eq for ScanlineEvent<T, ID> {}
//
// impl<T: PartialEq, ID> PartialEq for ScanlineEvent<T, ID> {
//     fn eq(&self, other: &Self) -> bool {
//         self.edge == other.edge && self.is_left_event == other.is_left_event
//     }
// }

impl<T: Num + Copy + Eq + PartialOrd, ID> Eq for ScanlineEvent<T, ID> {}

impl<T: Num + Copy + PartialEq + PartialOrd, ID> PartialEq for ScanlineEvent<T, ID> {
    fn eq(&self, other: &Self) -> bool {
        self.ordering_key() == other.ordering_key()
    }
}

impl<T: Num + Copy + PartialOrd, ID> ScanlineEvent<T, ID> {
    /// x coordinate of this event.
    fn current_x(&self) -> T {
        self.current_x_and_other_x().0
    }

    /// x coordinate of the other event.
    fn other_x(&self) -> T {
        self.current_x_and_other_x().1
    }

    /// Get x coordinate of this event and x coordinate of the other event.
    fn current_x_and_other_x(&self) -> (T, T) {
        if self.is_left_event ^ (self.edge.x_start < self.edge.x_end) {
            (self.edge.x_end, self.edge.x_start)
        } else {
            (self.edge.x_start, self.edge.x_end)
        }
    }

    /// Get a tuple which is used to compute the ordering among events.
    fn ordering_key(&self) -> (T, T, bool, T, usize) {
        let (current_x, other_x) = self.current_x_and_other_x();

        // Extend the edge horizontally.
        let (current_x, other_x) = if current_x < other_x {
            (
                current_x - self.horizontal_extension,
                other_x + self.horizontal_extension,
            )
        } else {
            (
                current_x + self.horizontal_extension,
                other_x - self.horizontal_extension,
            )
        };

        (
            current_x,
            self.edge.y,
            self.is_left_event,
            other_x,
            self.event_pair_id,
        )
    }
}

impl<T: Num + Copy + Ord, ID> Ord for ScanlineEvent<T, ID> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.ordering_key().cmp(&other.ordering_key()).reverse() // Reverse the ordering to use a max-heap as priority queue.
    }
}

impl<T: Num + Copy + PartialOrd, ID> PartialOrd for ScanlineEvent<T, ID> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.ordering_key()
            .partial_cmp(&other.ordering_key())
            .map(|ordering| ordering.reverse()) // Reverse the ordering to use a max-heap as priority queue.
    }
}

/// Given the current state of a scan-line check the neighboring edges for rule violations.
pub(crate) struct EdgeRelationCheckEngine<T, EdgeChecker> {
    /// Maximal perpendicular distance at which DRC interactions can happen.
    pub cutoff_distance: T,
    /// Maximal projective distance at which DRC interactions can happen.
    pub cutoff_distance_projection: T,
    /// If `shielded` is set to true, DRC violations can only happen between direct neighbours.
    pub shielded: bool,
    /// Check engines for edge-to-edge relations.
    pub edge_checkers: Vec<EdgeChecker>,
}

impl<T, EdgeChecker> EdgeRelationCheckEngine<T, EdgeChecker>
where
    T: Num + Copy + Ord,
    EdgeChecker: EdgeInteractionChecker<Edge = db::REdge<T>>,
{
    /// Return a tuple of iterators over the previous edges to `event` in the scanline and next edges in the scanline.
    fn get_scanline_neighbours<'a, ID>(
        &'a self,
        event: &'a ScanlineEvent<T, ID>,
        scanline: &'a BTreeSet<ScanlineEvent<T, ID>>,
    ) -> (
        impl Iterator<Item = &'a ScanlineEvent<T, ID>> + 'a,
        impl Iterator<Item = &'a ScanlineEvent<T, ID>> + 'a,
    ) {
        let max_num_neighbours = if self.shielded { 1 } else { scanline.len() };

        // Get neighbour edges in ascending y direction.
        let next_edges = scanline
            .range((Bound::Excluded(event), Bound::Unbounded))
            // Take only edges which are close enough to possibly interact.
            .take_while(|e| {
                let distance = e.edge.y - event.edge.y;
                let neg_distance = event.edge.y - e.edge.y;
                distance <= self.cutoff_distance && neg_distance <= self.cutoff_distance
            })
            .take(max_num_neighbours);

        // Get neighbour edges in descending y direction.
        let previous_edges = scanline
            .range((Bound::Unbounded, Bound::Excluded(event)))
            .rev()
            // Take only edges which are close enough to possibly interact.
            .take_while(|e| {
                let distance = e.edge.y - event.edge.y;
                let neg_distance = event.edge.y - e.edge.y;
                distance <= self.cutoff_distance && neg_distance <= self.cutoff_distance
            })
            .take(max_num_neighbours);

        (previous_edges, next_edges)
    }

    /// Check `event` for DRC violations with it's neighbour edges in the scanline.
    fn check_neighbours_drc<'a, F, ID>(
        &'a self,
        event: &'a ScanlineEvent<T, ID>,
        scanline: &'a BTreeSet<ScanlineEvent<T, ID>>,
        process_neighbours: &mut F,
    ) where
        F: FnMut(&db::REdge<T>, &db::REdge<T>),
    {
        debug_assert!(
            event.is_left_event,
            "scanline should only contain left events"
        );

        let (previous_edges, next_edges) = self.get_scanline_neighbours(event, scanline);

        let neighbour_edges = previous_edges.chain(next_edges).map(|e| &e.edge);

        // Run all check engines on all neighbours.
        neighbour_edges.for_each(move |neighbour| {
            // Convert horizontal edge to REdge.
            let edge: db::REdge<_> = (&event.edge).into();
            let neighbour_edge: db::REdge<_> = neighbour.into();

            process_neighbours(&edge, &neighbour_edge);
            process_neighbours(&neighbour_edge, &edge);
        })
    }

    /// Do DRC checks on a set of edges.
    pub fn check_drc_on_edges<Edges, EdgeId>(&self, edges: Edges) -> Vec<DrcMarker<db::REdge<T>>>
    where
        Edges: IntoIterator<Item = (EdgeId, db::REdge<T>)>,
        T: CoordinateType,
        EdgeId: Clone,
    {
        let (horizontal_edges, vertical_edges): (Vec<_>, Vec<_>) = edges
            .into_iter()
            .partition(|(_id, e)| e.orientation.is_horizontal());

        // Convert to horizontal edges.
        let horizontal_edges = horizontal_edges
            .into_iter()
            .map(|(id, e)| HorizontalEdge::new_with_id(e.start, e.end, e.offset, id));

        let vertical_edges = vertical_edges.into_iter().map(|(id, e)| {
            // Rotate vertical edge by -90 degrees.
            let e = e.rotate_ortho(db::Angle::R270);
            HorizontalEdge::new_with_id(e.start, e.end, e.offset, id)
        });

        let mut drc_markers = Vec::new();

        self.scan_horizontal_edges(horizontal_edges, &mut |edge, neighbour_edge| {
            let markers = self
                .edge_checkers
                .iter()
                .filter_map(|check_engine| check_engine.check(&edge, &neighbour_edge));

            drc_markers.extend(markers);
        });

        self.scan_horizontal_edges(vertical_edges, &mut |edge, neighbour_edge| {
            // Make edges vertical again.
            let edge = edge.rotate_ortho(db::Angle::R90);
            let neighbour_edge = neighbour_edge.rotate_ortho(db::Angle::R90);
            debug_assert!(edge.orientation.is_vertical());
            debug_assert!(neighbour_edge.orientation.is_vertical());

            let markers = self
                .edge_checkers
                .iter()
                .filter_map(|check_engine| check_engine.check(&edge, &neighbour_edge));

            drc_markers.extend(markers);
        });

        drc_markers
    }

    /// # Parameters
    /// * `edges`: Iterator over horizontal edges.
    fn scan_horizontal_edges<HorizontalEdges, ID, F>(
        &self,
        edges: HorizontalEdges,
        process_neighbours: &mut F,
    ) where
        HorizontalEdges: IntoIterator<Item = HorizontalEdge<T, ID>>,
        ID: Clone,
        F: FnMut(&db::REdge<T>, &db::REdge<T>),
    {
        let sorted_events = fill_event_queue(edges, self.cutoff_distance_projection);
        let mut scanline = BTreeSet::new();

        // Sweep through the events.
        for event in sorted_events.into_iter().rev() {
            if event.is_left_event {
                // Check the neighboring edges for DRC violations with the new edge.
                self.check_neighbours_drc(&event, &scanline, process_neighbours);

                // Insert event into scanline.
                scanline.insert(event);
            } else {
                // Right event is encountered.
                // Remove the left event from scanline.
                let left_event = ScanlineEvent {
                    is_left_event: true,
                    ..event
                };

                let remove_result = scanline.remove(&left_event);
                debug_assert!(remove_result, "left event is not in scanline");

                if self.shielded {
                    // Removal of an edge turns new edges into neighbours.
                    // Check the new neighbours for DRC violations.

                    let (mut previous_edges, _) =
                        self.get_scanline_neighbours(&left_event, &scanline);

                    if let Some(prev_event) = previous_edges.next() {
                        self.check_neighbours_drc(prev_event, &scanline, process_neighbours);
                    }
                } else {
                    // In non-shielded mode the new neighbours have already been checked for violations.
                }
            }
        }
    }
}

/// Initialize the event queue from an iterator over edges.
fn fill_event_queue<T, Edges, ID>(
    edges: Edges,
    cutoff_distance_projective: T,
) -> Vec<ScanlineEvent<T, ID>>
where
    T: Num + Copy + Ord,
    Edges: IntoIterator<Item = HorizontalEdge<T, ID>>,
    ID: Clone,
{
    let _1 = T::one();
    let _2 = _1 + _1;
    let extension = (cutoff_distance_projective + _1) / _2;

    // Create scan-line events from the edges.
    let events = edges
        .into_iter()
        .enumerate()
        .flat_map(|(event_pair_id, edge)| {
            let left_event = ScanlineEvent {
                edge: edge.clone(),
                horizontal_extension: extension,
                is_left_event: true,
                event_pair_id,
            };

            let right_event = ScanlineEvent {
                edge,
                horizontal_extension: extension,
                is_left_event: false,
                event_pair_id,
            };

            std::iter::once(left_event).chain(std::iter::once(right_event))
        });

    let mut events = Vec::from_iter(events);

    events.sort();

    events
}

#[test]
fn test_scanline_event_equality() {
    let event1 = ScanlineEvent {
        edge: HorizontalEdge::new(0, 1, 0),
        horizontal_extension: 0,
        is_left_event: true,
        event_pair_id: 1,
    };
    let event2 = ScanlineEvent {
        edge: HorizontalEdge::new(1, 0, 0), // Swapped start and end.
        horizontal_extension: 0,
        is_left_event: true,
        event_pair_id: 1,
    };
    assert_eq!(event1.ordering_key(), event2.ordering_key());
}

#[test]
fn test_event_queue_ordering() {
    let mut queue = fill_event_queue(
        vec![
            HorizontalEdge::new(0, 1, 0),
            HorizontalEdge::new(0, 2, 0),
            HorizontalEdge::new(1, 2, 0),
            HorizontalEdge::new(0, 2, 1),
        ],
        0,
    );

    let sorted_edges_by_left_point: Vec<_> = (0..queue.len())
        .flat_map(|_| queue.pop())
        .filter(|e| e.is_left_event)
        .map(|e| e.edge)
        .collect();

    let expected = vec![
        HorizontalEdge::new(0, 1, 0),
        HorizontalEdge::new(0, 2, 0),
        HorizontalEdge::new(0, 2, 1),
        HorizontalEdge::new(1, 2, 0),
    ];

    assert_eq!(sorted_edges_by_left_point, expected)
}

#[test]
fn test_scanline_drc_check_spacing_opposing_faces() {
    use super::edge_checks::EdgeRelation;
    use super::redge_checks::REdgeInteractionChecker;
    use iron_shapes::prelude::IntoEdges;

    // Check the spacing between two rectangles arranged as below:
    // ```txt
    // +---+
    // |   |
    // +---+
    //         -- spacing violation should be between rectangles
    // +---+
    // |   |
    // +---+
    // ```

    let r1 = db::Rect::new((0i32, 0), (10, 10));
    let r2 = db::Rect::new((0, 11), (20, 20));

    // Associate edges with polygon ID.
    let edges = r1
        .into_edges()
        .map(|e| (1, e))
        .chain(r2.into_edges().map(|e| (2, e)));

    let spacing_checker = REdgeInteractionChecker {
        relation_type: EdgeRelation::Spacing,
        orthogonal_distance: 2i32,
        projected_distance: 2,
    };

    let drc_engine = EdgeRelationCheckEngine {
        cutoff_distance: spacing_checker.orthogonal_distance * 2,
        cutoff_distance_projection: spacing_checker.projected_distance,
        shielded: false,
        edge_checkers: vec![spacing_checker],
    };

    let drc_markers = drc_engine.check_drc_on_edges(edges);

    dbg!(&drc_markers);

    assert_eq!(drc_markers.len(), 2);
}

#[test]
fn test_scanline_drc_check_spacing() {
    use super::edge_checks::EdgeRelation;
    use super::redge_checks::REdgeInteractionChecker;
    use iron_shapes::prelude::IntoEdges;

    // Check the spacing between two rectangles arranged as below:
    // ```txt
    //       +---+
    //       |   |
    //       +---+
    //
    // +---+
    // |   |
    // +---+
    // ```

    let r1 = db::Rect::new((0i32, 0), (10, 10));
    let r2 = db::Rect::new((19, 19), (29, 29));

    // Associate edges with polygon ID.
    let edges = r1
        .into_edges()
        .map(|e| (1, e))
        .chain(r2.into_edges().map(|e| (2, e)));

    let spacing_checker = REdgeInteractionChecker {
        relation_type: EdgeRelation::Spacing,
        orthogonal_distance: 10i32,
        projected_distance: 10,
    };

    let drc_engine = EdgeRelationCheckEngine {
        cutoff_distance: spacing_checker.orthogonal_distance * 2,
        cutoff_distance_projection: spacing_checker.projected_distance,
        shielded: false,
        edge_checkers: vec![spacing_checker],
    };

    let drc_markers = drc_engine.check_drc_on_edges(edges);

    dbg!(&drc_markers);

    assert_eq!(drc_markers.len(), 4);
}

#[test]
fn test_scanline_drc_check_width_simple() {
    use super::edge_checks::EdgeRelation;
    use super::redge_checks::REdgeInteractionChecker;
    use iron_shapes::prelude::IntoEdges;

    let r1 = db::Rect::new((0i32, 0), (1, 10));

    // Associate edges with polygon ID.
    let edges = r1.into_edges().map(|e| (1, e));

    let width_checker = REdgeInteractionChecker {
        relation_type: EdgeRelation::Width,
        orthogonal_distance: 2i32,
        projected_distance: 2,
    };

    let drc_engine = EdgeRelationCheckEngine {
        cutoff_distance: width_checker.orthogonal_distance * 2,
        cutoff_distance_projection: width_checker.projected_distance,
        shielded: false,
        edge_checkers: vec![width_checker],
    };

    let drc_markers = drc_engine.check_drc_on_edges(edges);

    dbg!(&drc_markers);

    assert_eq!(drc_markers.len(), 2);
}

// #[test]
// fn test_scanline_drc_check_width_unmerged() {
//     use iron_shapes::prelude::IntoEdges;
//     use super::redge_checks::REdgeInteractionChecker;
//     use super::edge_checks::EdgeRelation;
//
//     let r1_1 = db::Rect::new((0i32, 0), (1, 10));
//     let r1_2 = db::Rect::new((1i32, 0), (10, 10));
//
//     // Associate edges with polygon ID.
//     let edges = r1_1.into_edges().map(|e| (1, e))
//         .chain(r1_2.into_edges().map(|e| (2, e)));
//
//     let width_checker = REdgeInteractionChecker {
//         relation_type: EdgeRelation::Width,
//         orthogonal_distance: 2i32,
//         projected_distance: 2
//     };
//
//     let drc_engine = EdgeRelationCheckEngine {
//         cutoff_distance: width_checker.orthogonal_distance*2,
//         cutoff_distance_projection: width_checker.projected_distance,
//         shielded: false,
//         edge_checkers: vec![width_checker]
//     };
//
//     let drc_markers = drc_engine.check_drc_on_edges(edges);
//
//     dbg!(&drc_markers);
//
//     assert_eq!(drc_markers.len(), 0);
//
// }
