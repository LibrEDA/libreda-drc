// SPDX-FileCopyrightText: 2022 Thomas Kramer
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! DRC engine which supports simple spacing checks.

use super::*;
use db::traits::BoundingBox;
use edge_checks::EdgeRelation;
use num_traits::{Num, Zero};
use redge_checks::REdgeInteractionChecker;
use scanline::EdgeRelationCheckEngine;

/// DRC engine which supports simple spacing checks.
/// Requires the input polygons to be merged.
pub struct SimpleDrcEngine<'a, Rules> {
    rules: &'a Rules,
    /// Do spacing checks on metal layers.
    enable_spacing_checks: bool,
    /// Enclosure checks between vias and metal layers.
    enable_via_enclosure_checks: bool,
}

impl<'a, Rules> SimpleDrcEngine<'a, Rules> {
    /// Create a new DRC check engine based on the given design rules.
    pub fn new(rules: &'a Rules) -> Self {
        Self {
            rules,
            enable_spacing_checks: true,
            enable_via_enclosure_checks: true,
        }
    }

    /// Enable/disable spacing checks of metal layers. Enabled by default.
    pub fn enable_spacing_check(&mut self, enable: bool) {
        self.enable_via_enclosure_checks = enable;
    }
}

impl<'a, LN, Rules> DrcEngine<LN> for SimpleDrcEngine<'a, Rules>
where
    LN: db::L2NBase,
    Rules: db::RuleBase<LayerId = LN::LayerId>
        + db::DistanceRuleBase<Distance = LN::Coord>
        + db::MinimumSpacing
        + db::MinimumWidth,
    LN::Coord: Num + Ord,
{
    type DrcResult = SimpleDrcResult<LN>;
    type Error = ();

    fn check(_chip: &LN, _top_cell: &LN::CellId) -> Result<Self::DrcResult, Self::Error> {
        todo!()
    }

    fn flat_check(
        &self,
        layer_data: &HashMap<LN::LayerId, Vec<(db::SimpleRPolygon<LN::Coord>, Option<LN::NetId>)>>,
    ) -> Result<Self::DrcResult, Self::Error> {
        let mut drc_result = SimpleDrcResult { markers: vec![] };

        // Spacing checks.
        if self.enable_spacing_checks {
            // Check layer by layer.
            for (layer, shapes) in layer_data {
                // Associate edges with polygon ID.
                let all_edges = shapes
                    .iter()
                    .enumerate()
                    .flat_map(|(idx, (poly, _net))| poly.edges().map(move |e| (idx, e)));

                if let Some(min_spacing) = self.rules.min_spacing_absolute(layer) {
                    // If a spacing rule is defined, then:
                    let spacing_checker = REdgeInteractionChecker {
                        relation_type: EdgeRelation::Spacing,
                        orthogonal_distance: min_spacing,
                        projected_distance: min_spacing,
                    };

                    let half_cutoff = spacing_checker.orthogonal_distance;

                    let edge_check_engine = EdgeRelationCheckEngine {
                        cutoff_distance: half_cutoff + half_cutoff,
                        cutoff_distance_projection: spacing_checker.projected_distance,
                        shielded: false,
                        edge_checkers: vec![spacing_checker],
                    };

                    let drc_markers = edge_check_engine.check_drc_on_edges(all_edges);

                    // Convert markers to rectangles.
                    let rectangle_markers = drc_markers.into_iter().filter_map(|marker| {
                        let rect = marker
                            .edge1
                            .bounding_box()
                            .add_rect(&marker.edge1.bounding_box());

                        if rect.width() * rect.height() > LN::Coord::zero() {
                            Some((layer.clone(), rect))
                        } else {
                            None
                        }
                    });

                    drc_result.markers.extend(rectangle_markers);
                }
            }
        }

        Ok(drc_result)
    }
}

/// Design-rule check result of the [`SimpleDrcEngine`].
pub struct SimpleDrcResult<LN: db::L2NBase> {
    markers: Vec<(LN::LayerId, db::Rect<LN::Coord>)>,
}

impl<LN: db::L2NBase> DrcResult<LN> for SimpleDrcResult<LN> {
    fn num_violations(&self) -> usize {
        self.markers.len()
    }

    fn markers(&self) -> Vec<(LN::LayerId, db::Rect<LN::Coord>)> {
        self.markers.clone()
    }
}
