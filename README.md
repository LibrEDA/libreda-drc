<!--
SPDX-FileCopyrightText: 2022 Thomas Kramer

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# LibrEDA DRC

Simple design rule checker for the [LibrEDA](https://libreda.org) framework.

This DRC library is currently developed to be used for a detail router.

# Documentation

Documentation is integrated in the code. Use `cargo doc --open` to view it.
